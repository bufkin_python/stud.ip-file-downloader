# Stud.IP File Downloader

> **_Ich selbst kann das Projekt nicht mehr weiter entwickeln, da ich keinen Zugriff mehr auf Stud.IP habe._**

Dieses Skript lädt automatisch alle Dateien aus den gewählten Fächern von Stud.IP der Hochschule Trier herunter und entpackt diese schließlich in den richtigen Ordner.

Die URLs zum "Dateien-Tab" der Fächer müssen in der Datei "settings.ini" gesetzt werden.

## Voraussetzungen

Damit das Skript sich selbstständig einloggen kann, müssen die Zugangsdaten zu Stud.IP in den Umgebungsvariablen von Windows gespeichert werden. Falls eine Linux-Distribution wie zum Beispiel Ubuntu verwendet wird, müssen die beiden Variablen wie folgt in der Datei .profile im Verzeichnis /etc/skel gesetzt werden:

- export StudIP_User='Benutzername'
- export StudIP_Pass='Passwort'

Die Variablen müssen dabei in beiden Fällen folgendermaßen benannt werden:

- StudIP_User
- StudIP_Pass

---

Zudem wird der Internetbrowser Firefox benötigt, da das Skript diesen versuchen wird aufzurufen.

## Noch nicht implementierte Funktionen

- Abfragen der Benutzerdaten beim ersten Starten des Skripts und Speichern dieser in den Umgebungsvariablen
