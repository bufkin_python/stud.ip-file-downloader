import os
import time
from configparser import ConfigParser
from os import listdir, makedirs, remove
from os.path import isfile, join

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.common.by import By


def find_all_files():
    """ Find all files in the download path set by the user. """
    print("Searching for downloaded files.")
    only_files = []
    for f in listdir(main.download_path):
        if isfile(join(main.download_path, f)):
            only_files.append(f)
    return only_files


def read_settings():
    """ Reads the settings in the set folder and adds a missing slash to the path. """
    if not (main.download_path.endswith("/")):
        main.download_path += "/"

    print("Creating temporary download folder.")
    makedirs(main.download_path, exist_ok=True)


def determine_extract_path(file_name: str):
    """ Determined the extraction path set in the settings file. """
    print("Extracting files into correct location.")

    file_name = file_name.replace("Tutorium", "")
    file_name = file_name.replace("Vorlesung", "")
    file_name = file_name.replace("_", " ")
    file_name = file_name.replace(".zip", "")
    file_name.strip()


class Main:
    """ Main class of this project. """

    options = ChromeOptions()
    driver = webdriver.Chrome(options=options)

    def __init__(self):
        self.extract_path = None
        self.file_list = None
        config = ConfigParser()
        config.read("settings.ini")

        self.download_path = config["Speicherort"]["Download_Pfad"]
        self.root_path = config["Speicherort"]["Pfad"]

        self.urls = config.get("Zu ladende URLs", "URLs").split("\n")

    def login_to_site(self):
        """ Logins to the Stud.IP with the credentials set in the path variables. """
        print("Logging into Stud.IP.")
        element = self.driver.find_element(By.XPATH,
                                           "/html/body/div[1]/div[3]/div/div[3]/nav/div[1]/a"
                                           )
        element.click()
        element = self.driver.find_element(By.XPATH, '//*[@id="username"]')
        element.send_keys(user_name)
        element = self.driver.find_element(By.XPATH,
                                           '/html/body/div[2]/div/div/div/form/div[1]/table/tbody/tr[3]/td[2]/input'
                                           )
        element.send_keys(user_password)
        element.send_keys(Keys.RETURN)

    def select_everything(self):
        """ Selects everything on the webpage. """
        print("Selecting all files.")
        time.sleep(1)
        element = self.driver.find_element(By.NAME, "all_files_checkbox")
        element.click()

    def download_everything(self):
        """ Downloads everything from the webpage. """
        print("Downloading all files.")
        time.sleep(1)
        element = self.driver.find_element(By.NAME, "download")
        element.click()

    def empty_download_folder(self, _path: str):
        """ Empties the download folder set in the settings file. """
        print("Deleting temporarily saved files.")
        for file_ in self.file_list:
            remove(_path + file_)


if __name__ == "__main__":
    main = Main()
    read_settings()

    options = ChromeOptions()
    options.headless = False
    options.set_preference("browser.download.folderList", 2)
    options.set_preference("profile.default_content_settings.popups", 0)
    options.set_preference(
        "download.default_directory", main.download_path)
    options.set_preference("directory_upgrade", True)
    options.set_preference(
        "browser.helperApps.neverAsk.saveToDisk", "application/zip")
    options.set_preference(
        "browser.helperApps.neverAsk.openFile", "application/zip")

    user_name = os.environ.get("StudIP_User")
    user_password = os.environ.get("StudIP_Pass")

    fp = webdriver.Chrome()

    # binary = FirefoxBinary(r"C:/Users/sebi/dev/stud.ip-file-downloader/chromedriver.exe")
    main.driver = webdriver.Chrome(
        options=options,
        chrome_binary=None,
        chrome_profile=fp,
        service_log_path="./logfile.log",
    )

# if __name__ == '__main__':
#     with cProfile.Profile() as pr:
#         main = Main()
#         read_settings()

#     options = FirefoxOptions()
#     options.add_argument("--headless")

#     profile = FirefoxProfile()
#     user_name = os.environ.get("StudIP_User")
#     user_password = os.environ.get("StudIP_Pass")

#     fp = webdriver.FirefoxProfile()
#     fp.set_preference("browser.download.folderList", 2)
#     fp.set_preference("profile.default_content_settings.popups", 0)
#     fp.set_preference("download.default_directory", main.download_path)
#     fp.set_preference("directory_upgrade", True)
#     fp.set_preference(
#         "browser.helperApps.neverAsk.saveToDisk", "application/zip")
#     fp.set_preference("browser.helperApps.neverAsk.openFile",
#                       "application/zip")

#     binary = FirefoxBinary(r"C:/Program Files/Mozilla Firefox/Firefox.exe")
#     main.driver = webdriver.Firefox(
#         options=options,
#         firefox_binary=binary,
#         firefox_profile=fp,
#         service_log_path="./logfile.log",
#     )

#     main.driver.get("https://studip.hochschule-trier.de")

#     for index, url in enumerate(main.urls):
#         if index == 0:
#             main.login_to_site()

#         main.driver.get(url)
#         main.select_everything()
#         main.download_everything()

#     main.driver.close()
#     find_all_files()

#     for file in main.file_list:
#         with ZipFile(main.download_path + file, "r") as zipObj:
#             determine_extract_path(file)
#             zipObj.extractall(main.extract_path)

#     main.empty_download_folder(main.download_path)

# stats = pstats.Stats(pr)
# stats.sort_stats(pstats.SortKey.TIME)
